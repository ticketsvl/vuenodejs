const axios = require('axios')
require('dotenv').config()

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'ru-RU',
    },
    title: 'Фронтальные погрузчики и экскаваторы Болдер | Купить заказать в лизинг. Цены, фото',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1.0, minimal-ui'},
      {hid: 'description', name: 'description', content: 'Ищите фронтальный, вилочный погрузчик или экскаватор? Болдер (Boulder) это новые российские экскаваторы и колесные погрузчики с отличными характеристиками по недорогой цене.'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css'},
      //{rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Ubuntu:300,400,700&display=swap&subset=cyrillic-ext'},
      //{rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:300,400,600,700&display=swap'}
    ],
    script: [
      {src: '/js/marzipano.js'},
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#FB1C1C'},

  router: {
    // ran before every route on both client and server
    middleware: ['myMiddleware']
  },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {src: '~/plugins/swiper.js', mode: 'client'},
    {src: '~/plugins/editor.js', mode: 'client'},
    {src: '~/plugins/datepicker.js', mode: 'client'},
    {src: '~/plugins/suggestions.js', mode: 'client'},
    {src: '~/plugins/vue-the-mask.js', mode: 'client'},
    {src: '~/plugins/vuelidate.js', mode: 'client'},
    {src: '~/plugins/numbers-animate.js', mode: 'client'},
    {src: '~/plugins/ymapPlugin.js', mode: 'client'},
    {src: '~/plugins/vue360.js', mode: 'client'},
    {src: '~/plugins/touch.js', mode: 'client'},
    {src: '~/plugins/share.js', mode: 'client'},
    {src: '~/plugins/viewport.js', mode: 'client'},
    {src: '~/plugins/axios.js'},
    {src: '~/plugins/scroll-animate.js'},
    {src: '~/plugins/vue-swal'},
    {src: '~/plugins/scrollbar.js'},
    {src: '~/plugins/collapse.js'},
    {src: '~/plugins/vue-video.js'},
    {src: '~/plugins/gallery.js', mode: 'client'},
    {src: '~/plugins/lingallery.js', mode: 'client'},
    {src: '~/plugins/carousel.js', mode: 'client'},
  ],
  // some nuxt config...
  css: [
    '@/assets/style.less',
    '@/assets/plugins-style.css',
    '@/assets/fonts-style.css',
    'swiper/dist/css/swiper.css',
    'perfect-scrollbar/css/perfect-scrollbar.css',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Simple usage
    '@nuxtjs/dotenv'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    'nuxt-bootstrap-slider',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/gtm',
    ['nuxt-canonical', { baseUrl: process.env.BASE_URL}],
    ["nuxt-compress", {
        gzip: {
          cache: true
        },
        brotli: {
          threshold: 10240
        }
      }
    ],
    // Обязательно в конце
    '@nuxtjs/sitemap'
  ],
  gtm: {
    id: 'GTM-PZFZJ5R',
    enabled: true,
    debug: true,
    pageTracking: true,
    pageViewEventName: 'pageView',
  },
  axios: {
    baseURL: process.env.BASE_URL
  },

  sitemap: {
    exclude: [
        '/admin',
        '/admin/**',
        '/pre-catalog'
    ],
    routes: async () => {
      let baseUrl = process.env.BASE_URL,
          staticRoutes = [
        '/catalog',
        '/catalog/backhoe',
        '/catalog/excavator',
        '/catalog/frontloaders',
        '/catalog/forkloaders',
      ],
          newData = [],
          data,

          newGroup = [],
          alreadyAdded = [],
          group,
          attachments,
          attIds,

          news,
          articles,
          video,
          questions

      try {
        await axios.post(`https://epc.kuzmin.online/epcapi/vehicles/list`, {
          "Page": 1,
          "Rows": 1000,
        }).then(function (resolve) {
          data = resolve.data.Payload.Data
        })
      } catch (e) {
        console.log(e)
      }
      try {
        await axios.post(`https://epc.kuzmin.online/epcapi/parts_groups_features/list`, {
          'Page': 1,
          'Rows':10000,
        }).then(function (resolve) {
          group = resolve.data.Payload.Data
          attIds = group.map(function(item){
            return item.PartsGroupsId
          })
        })
      } catch (e) {
        console.log('e', e)
      }
      try {
        await axios.post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
          'Page': 1,
          'Rows':10000,
          filter: [
            {
              'Field':'Id',
              'Operator': 14,
              'Value': attIds
            }
          ]
        }).then(function (resolve) {
          attachments = resolve.data.Payload.Data
        })
      } catch (e) {
        console.log('e', e)
      }
      group.forEach(await function(item){
        attachments.forEach(function(itemInner){
          if (item.PartsGroupsId === itemInner.Id){
            item.PartsCategoryId = itemInner.PartsCategoryId
            item.NameRus = itemInner.NameRus
            item.NameEng = itemInner.NameEng
            item.NameChi = itemInner.NameChi
          }
        })
      })
      group.forEach(function(item){
        if (alreadyAdded.indexOf(item.NameRus) === -1){
          alreadyAdded.push(item.NameRus)
          console.log(item)
          newGroup.push(`/attachment/${item.NameEng.replace(/\s/g, "_").toLowerCase()}`)
        }
      })
      data.forEach(await function(item){
        if (item.VehicleTypeId === 1376757) {
          newData.push(`/catalog/frontloaders/${item.Name.toLowerCase()}`)
        }
        if (item.VehicleTypeId === 1376760) {
          newData.push(`/catalog/forkloaders/${item.Name.toLowerCase()}`)
        }
        if (item.VehicleTypeId === 1376758) {
          newData.push(`/catalog/backhoe/${item.Name.toLowerCase()}`)
        }
        if (item.VehicleTypeId === 1376759) {
          newData.push(`/catalog/excavator/${item.Name.toLowerCase()}`)
        }
      })
      try{
        let newsArray = await axios.get(`${baseUrl}/api/media/news`)
        news =  newsArray.data.map((item) => `/news/${item.newsUrl}`)
      } catch(e) {
        console.log(e)
      }
      try{
        let articlesArray = await axios.get(`${baseUrl}/api/media/articles`)
        articles =  articlesArray.data.map((item) => `/articles/${item.newsUrl}`)
      } catch(e) {
        console.log(e)
      }
      try{
        let videoArray = await axios.get(`${baseUrl}/api/media/video`)
        video =  videoArray.data.map((item) => `/video/${item.pageUrl}`)
      } catch(e) {
        console.log(e)
      }
      try{
        let questionsArray = await axios.get(`${baseUrl}/api/media/questions`)
        questions =  questionsArray.data.map((item) => `/questions/${item.url}`)
      } catch(e) {
        console.log(e)
      }
      return newData.concat(staticRoutes, news, articles, video, questions, newGroup)
    }
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: [
      'vue-swal',
    ],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  telemetry: false,
  scrollBehavior: function (to, from, savedPosition) {
    return {x: 0, y: 0}
  },
}
