export const state = () => ({
})
export const actions = {
	async fetchAdminProducts({commit}) {
		try{
			const products = await this.$axios.$get('/api/product/admin')
			return products
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchProducts({commit}) {
		try{
			const products = await this.$axios.$get('/api/product/')
			return products
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async getImgs({commit}, name) {
		try{
			const number = await this.$axios.$get(`/api/product/img/${name}`)
			return number
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchOptions({commit}) {
		try{
			const options = await this.$axios.$get('/api/product/options')
			return options
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchProductsByType({commit}, type) {
		try{
			return await this.$axios.$get(`/api/product/catalog/${type}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	//Получаем изображение по типу и id
	async getImgByIdAndType({commit}, data) {
		let img
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/catalog_images/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Source',
						'Operator': 14,
						'Value': [data.type]
					},
					{

						'Field':'SourceId',
						'Operator': 14,
						'Value': [data.VehicleId]
					}
				]
			}).then(function (resolve) {
				img = resolve.Payload.Data
			})
			return img
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async fetchProductsByTypeEpc({commit}, name) {
		let vehicle
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/vehicles/list`, {
				"Page": 1,
				"Rows": 100,
				filter: [
					{
						'Field':'Name',
						'Value': name
					}
				]
			}).then(function (resolve) {
				vehicle = resolve.Payload.Data
			})
			return vehicle[0]
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getCharForModelById({commit}, VehicleId) {
		let chars
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/vehicles_features/list`, {
				"Page": 1,
				"Rows": 100,
				filter: [
					{
						'Field':'VehicleId',
						'Value': VehicleId
					}
				]
			}).then(function (resolve) {
				chars = resolve.Payload.Data
			})
			return chars
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAttachmentInfoForModelById({commit}, id) {
		let attachments
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'VehicleId',
						'Operator': 14,
						'Value': [id]
					},
				]
			}).then(function (resolve) {
				attachments = resolve.Payload.Data
			})
			return attachments
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAttachmentForModelById({commit}, VehicleId) {
		let attachments
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/mods_parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'VehicleId',
						'Operator': 14,
						'Value': [VehicleId]
					},
					{

						'Field':'PartsCategoryId',
						'Operator': 14,
						'Value': [27,29,30,31,32,33,34,35,37,38,39,42]
					}
				]
			}).then(function (resolve) {
				attachments = resolve.Payload.Data
			})
			return attachments
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAllGroupTypes({commit}) {
		let groups
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups_features/list`, {
				'Page': 1,
				'Rows':10000,
			}).then(function (resolve) {
				groups = resolve.Payload.Data
			})
			return groups
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAllGroupTypesByNameEng({commit}, nameEng) {
		let group
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
				'Page': 1,
				'Rows':10000,
				filter: [
					{
						'Field':'NameEng',
						'Operator': 14,
						'Value': [nameEng]
					}
				]
			}).then(function (resolve) {
				group = resolve.Payload.Data
			})
			return group
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAllAttachments({commit}) {
		let attachments
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
				'Page': 1,
				'Rows':10000,
				filter: [
					{
						'Field':'PartsCategoryId',
						'Operator': 14,
						'Value': [27,29,30,31,32,33,34,35,37,38,39,42]
					}
				]
			}).then(function (resolve) {
				attachments = resolve.Payload.Data
			})
			return attachments
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAttachmentsByIds({commit}, ids) {
		let attachments
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
				'Page': 1,
				'Rows':10000,
				filter: [
					{
						'Field':'Id',
						'Operator': 14,
						'Value': ids
					}
				]
			}).then(function (resolve) {
				attachments = resolve.Payload.Data
			})
			return attachments
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getInfoForModelAdd({commit}, id) {
		let item
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/mods_parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Id',
						'Operator': 14,
						'Value': [id]
					},
				]
			}).then(function (resolve) {
				item = resolve.Payload.Data
			})
			return item
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAllGroupMoreInfo({commit}, ids) {
		let items
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/mods_parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'PartsGroupsId',
						'Operator': 14,
						'Value': ids
					},
				]
			}).then(function (resolve) {
				items = resolve.Payload.Data
			})
			return items
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getPartsByCodes({commit}, codes) {
		let items
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts/list`, {
				'Page':1,
				'Rows':9999,
				filter:[
					{
						'Field':'PartNumber',
						'Operator': 14,
						'Value': codes,
					}]
			}).then(function (resolve) {
				items = resolve.Payload.Data
			})
			return items
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getOptionsForModelById({commit}, VehicleId) {
		let options
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/mods_parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'VehicleId',
						'Operator': 14,
						'Value': [VehicleId]
					},
					{

						'Field':'PartsCategoryId',
						'Operator': 14,
						'Value': [25]
					}
				]
			}).then(function (resolve) {
				options = resolve.Payload.Data
			})
			return options
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getSetsForModelById({commit}, VehicleId) {
		let options
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/mods_parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'VehicleId',
						'Operator': 14,
						'Value': [VehicleId]
					},
					{

						'Field':'PartsCategoryId',
						'Operator': 14,
						'Value': [26]
					}
				]
			}).then(function (resolve) {
				options = resolve.Payload.Data
			})
			return options
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getOptOrAtachId({commit}, data) {
		let newId
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/mods_parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'PartsGroupsId',
						'Operator': 14,
						'Value': [data.id]
					},{
						'Field':'VehicleId',
						'Operator': 14,
						'Value': [data.vehicleId]
					},
				]
			}).then(function (resolve) {
				newId = resolve.Payload.Data[0].Id
			})
			return newId
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAttachmentsImages({commit}, obj) {
		let values = Object.values(obj)
		let attachmentImages
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/files/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Source',
						'Operator': 14,
						'Value': ['parts_groups']
					},
					{

						'Field':'SourceId',
						'Operator': 14,
						'Value': values
					}
				]
			}).then(function (resolve) {
				attachmentImages = resolve.Payload.Data
			})
			return attachmentImages
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getImage({commit}, id) {
		let img
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/files/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Source',
						'Operator': 14,
						'Value': ['parts_groups']
					},
					{

						'Field':'SourceId',
						'Operator': 14,
						'Value': [id]
					}
				]
			}).then(function (resolve) {
				img = resolve.Payload.Data
			})
			return img
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getImageByTypeAndId({commit}, data) {
		let type = data.type
		let id = data.id
		let img
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/files/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Source',
						'Operator': 14,
						'Value': type
					},
					{

						'Field':'SourceId',
						'Operator': 14,
						'Value': id
					}
				]
			}).then(function (resolve) {
				img = resolve.Payload.Data
			})
			return img
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getAllImages({commit}, array) {
		let img
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/files/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Source',
						'Operator': 14,
						'Value': ['parts_groups']
					},
					{

						'Field':'SourceId',
						'Operator': 14,
						'Value': array
					}
				]
			}).then(function (resolve) {
				img = resolve.Payload.Data
			})
			return img
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},

	async getItemInfo({commit}, id) {
		let description
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Id',
						'Operator': 14,
						'Value': [id]
					},
				]
			}).then(function (resolve) {
				description = resolve.Payload.Data
			})
			return description
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getItemInfoObj({commit}, id) {
		let obj
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/parts_groups/list`, {
				'Page': 1,
				'Rows':1000,
				filter: [
					{
						'Field':'Id',
						'Operator': 14,
						'Value': [id]
					},
				]
			}).then(function (resolve) {
				obj = resolve.Payload.Data
			})

			return obj
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async getFileIdForAttachmentSrc({commit}, id) {
		let attachmentSrc
		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/catalog_images/list`, {
				'Page': 1,
				'Rows':100,
				filter: [
					{
						'Field':'Source',
						'Operator': 14,
						'Value': ["parts_groups"]
					},
					{

						'Field':'SourceId',
						'Operator': 14,
						'Value': [id]
					}
				]
			}).then(function (resolve) {
				attachmentSrc = resolve.Payload.Data
			})
			return attachments
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async fetchByModel({commit}, model) {
		try{
			return await this.$axios.$get(`/api/product/model/${model}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchMoreInfoByModel({commit}, model) {
		try{
			return await this.$axios.$get(`/api/product/admin/moreinfo/${model}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async updateMoreInfoByModel({commit}, data) {
		try{
			return await this.$axios.$put(`/api/product/admin/moreinfo/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchAdminByModel({commit}, model) {
		try{
			return await this.$axios.$get(`/api/product/admin/products/${model}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async create({commit}, data) {
		try{
			return await this.$axios.$post('/api/product/admin', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async createMoreInfo({commit}, data) {
		try{
			return await this.$axios.$post(`/api/product/admin/moreinfo/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async createOption({commit}, data) {
		try{
			return await this.$axios.$post('/api/product/admin/option', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async update({commit}, formData) {
		try{
			return await this.$axios.$put(`/api/product/admin/products/${formData.model.toLowerCase()}`, formData)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async remove({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/product/admin/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
}

export const mutations = {
}

export const getters = {
}
