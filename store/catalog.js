export const state = () => ({
	catalogFilterOpened: false
})

export const actions = {

}
export const mutations = {
	catalogFilterToggle(state){
		state.catalogFilterOpened = !state.catalogFilterOpened
	}
}
export const getters = {
	catalogFilterOpened: state => state.catalogFilterOpened
}