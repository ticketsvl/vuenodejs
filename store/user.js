export const actions = {
	async fetchUsers({commit}) {
		try{
			const users = await this.$axios.$get('/api/user/admin')
			return users
		} catch(e) {
			commit('setError', e, {root:true})
		}
	}
}