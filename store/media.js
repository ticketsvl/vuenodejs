
export const state = () => ({
	news: null,
	questions: null,
	video: null
})
export const actions = {
	// NEWS ACTIONS
	async fetchNews({commit}) {
		try{
			const news = await this.$axios.$get('/api/media/news')
			commit('newsDataSet', news)
			return news
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchAdminNews({commit}) {
		try{
			const news = await this.$axios.$get('/api/media/admin/news')
			return news
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchNewsByUrl({commit}, id) {

		try{
			const newsById = await this.$axios.$get(`/api/media/news/${id}`)
			return newsById
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async addNews({commit}, data) {
		try{
			return await this.$axios.$post('/api/media/admin/news', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async updateNews({commit}, data) {
		try{
			return await this.$axios.$put(`/api/media/admin/news/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async removeNews({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/media/admin/news/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	// ARTICLES ACTIONS
	async fetchArticles({commit}) {

		try{
			const articles = await this.$axios.$get('/api/media/articles')
			return articles
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchAdminArticles({commit}) {

		try{
			const articles = await this.$axios.$get('/api/media/admin/articles')
			return articles
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchArticleByUrl({commit}, id) {

		try{
			const articleById = await this.$axios.$get(`/api/media/articles/${id}`)
			return articleById
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async addArticle({commit}, data) {
		try{
			return await this.$axios.$post('/api/media/admin/articles', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async updateArticle({commit}, data) {
		try{
			return await this.$axios.$put(`/api/media/admin/articles/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async removeArticle({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/media/admin/articles/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	// VIDEO ACTIONS
	async fetchVideo({commit}) {
		try{
			const video = await this.$axios.$get('/api/media/video')
			commit('videoDataSet', video)
			return video
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchVideoByUrl({commit}, id) {

		try{
			const videoById = await this.$axios.$get(`/api/media/video/${id}`)
			return videoById
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async addVideo({commit}, data) {
		try{
			return await this.$axios.$post('/api/media/admin/video', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async updateVideo({commit}, data) {
		try{
			return await this.$axios.$put(`/api/media/admin/video/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async removeVideo({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/media/admin/video/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	// QUESTIONS ACTIONS
	async fetchQuestionsById({commit}, id) {

		try{
			const questionsById = await this.$axios.$get(`/api/media/questions/${id}`)
			return questionsById
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchQuestions({commit}) {
		try{
			const questions = await this.$axios.$get('/api/media/questions')
			commit('questionsDataSet', questions)
			return questions
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async updateQuestion({commit}, data) {
		try{
			return await this.$axios.$put(`/api/media/admin/questions/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async removeQuestion({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/media/admin/questions/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async addQuestion({commit}, data) {
		try{
			return await this.$axios.$post('/api/media/admin/questions', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async addVacancy({commit}, data) {
		try{
			return await this.$axios.$post('/api/media/admin/vacancy', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async updateVacancy({commit}, data) {
		try{
			return await this.$axios.$put(`/api/media/admin/vacancy/`, data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchVacancyList({commit}) {
		try{
			const vacancyList = await this.$axios.$get('/api/media/vacancy')
			return vacancyList
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async fetchVacancyById({commit}, id) {
		try{
			const vacancy = await this.$axios.$get(`/api/media/vacancy/${id}`)
			return vacancy[0]
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async removeVacancy({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/media/admin/vacancy/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
}

export const mutations = {
	newsDataSet(state, payload) {
		state.news = payload
	},
	questionsDataSet(state, payload) {
		state.questions = payload
	},
	videoDataSet(state, payload) {
		state.video = payload
	},
}
export const getters = {
	news: state => state.news,
	questions: state => state.questions,
	video: state => state.video,
}
