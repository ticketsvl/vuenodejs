import Cookie from 'cookie'
import Cookies from 'js-cookie'

export const state = () => ({
	attachmentFilterCurrent: 0,
	menuState: false,
	preloader: true,
	allowScrolling: true,
	error: null,
	geoPosition: {
		x: null,
		y: null,
		city: null,
	},
	cookieCity: '',
	closestDealerCookies: [],
	cityCheckDone: false,
	closestDealer: null,
	modelPageTranslate: 0,
	setPositionDone: false,
	setClosestDealerDone: false,
	lastProductListMain: [],
	allVehiclesWithChars: null,
	videoBgReady: false,
	allDealers: null,
})

export const actions = {
	nuxtServerInit({dispatch}) {
		dispatch('fetchAllDealers')
		dispatch('auth/autoLogin')
		dispatch('checkCookieCity')
		dispatch('fetchAllVehiclesWithChars')
	},
	async fetchAllVehiclesWithChars({commit}){
		let vehiclesList
		let vehiclesChars
		let vehiclesImgData

		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/vehicles/list`, {
				"Page": 1,
				"Rows": 100,
			}).then(function (resolve) {
				vehiclesList = resolve.Payload.Data
			})
		} catch (e) {
			commit('setError', e, {root: true})
		}

		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/vehicles_features/list`, {
				"Page": 1,
				"Rows": 10000,
			}).then(function (resolve) {
				vehiclesChars = resolve.Payload.Data
			})
		} catch (e) {
			commit('setError', e, {root: true})
		}

		try {
			await this.$axios.$post(`https://epc.kuzmin.online/epcapi/catalog_images/list`, {
				"Page":1,
				"Rows":1000,
				filter:[{"Field":"Source", "Value":"vehicles"}]
			}).then(function (resolve) {
				vehiclesImgData = resolve.Payload.Data
			})
		} catch (e) {
			commit('setError', e, {root: true})
		}
		if (vehiclesList) {
			vehiclesList.forEach(await function(vehicleItem){
				vehicleItem.characteristics = []
				vehicleItem.catalogMainSpecifications = {
					bucketCapacity: '',
					carryingCapacity: '',
					weight: '',
					liftingHeight: ''
				}
				vehicleItem.extraType = {}

				vehiclesChars.forEach(function(charItem){
					if (vehicleItem.Id === charItem.VehicleId) {
						vehicleItem.characteristics.push(charItem)

						if(charItem.FeatureId === 104 ) {
							vehicleItem.extraType.full = charItem.ValueRus
							switch(charItem.ValueRus){
								case 'Электрические 3-х опорные':
									vehicleItem.extraType.short = 'Элек.3х опор.'
									break
								case 'Электрические штабелеры (ричтраки)':
									vehicleItem.extraType.short = 'Элек.штабелеры'
									break
								case 'Газ-бензиновые':
									vehicleItem.extraType.short = 'Газ-бенз.'
									break
								case 'Электрические':
									vehicleItem.extraType.short = 'Элек.'
									break
							}
						}
						if(charItem.FeatureId === 8) {
							vehicleItem.catalogMainSpecifications.bucketCapacity = charItem.ValueRus.replace("м3", '')
						}
						if(charItem.FeatureId === 11) {
							vehicleItem.catalogMainSpecifications.carryingCapacity = charItem.ValueRus.replace("кг", '')
						} else if (charItem.FeatureId === 52) {
							vehicleItem.catalogMainSpecifications.carryingCapacity = charItem.ValueRus.replace("кг", '')
						}
						if(charItem.FeatureId === 10) {
							vehicleItem.catalogMainSpecifications.weight = charItem.ValueRus.replace("кг", '')
						} else if (charItem.FeatureId === 73) {
							vehicleItem.catalogMainSpecifications.weight = charItem.ValueRus.replace("кг", '')
						} else if (charItem.FeatureId === 37) {
							vehicleItem.catalogMainSpecifications.weight = charItem.ValueRus.replace("кг", '')
						}
						if(charItem.FeatureId === 27) {
							vehicleItem.catalogMainSpecifications.liftingHeight = charItem.ValueRus.replace("мм", '')
						}
						if( vehicleItem.VehicleTypeEng === "Mini excavators"){
							if(charItem.FeatureId === 24) {
								vehicleItem.catalogMainSpecifications.bucketCapacity = charItem.ValueRus.replace("мм", '')
							}
							if(charItem.FeatureId === 37) {
								vehicleItem.catalogMainSpecifications.weight = charItem.ValueRus.replace("кг", '')
							}
							if(charItem.FeatureId === 23) {
								vehicleItem.catalogMainSpecifications.carryingCapacity = charItem.ValueRus.replace("мм", '')
							}

						}
					}
				})

				vehiclesImgData.forEach(function(imgItem){
					if ( vehicleItem.Id === imgItem.SourceId ) {
						vehicleItem.imgId = imgItem.FileId
					}
				})
			})
			commit('allVehiclesWithCharsSet', vehiclesList)
		} else vehiclesList = []
		return vehiclesList
	},
	checkCookieCity({commit}) {
		const cookieStr = process.browser
			? document.cookie
			: this.app.context.req.headers.cookie

		const cookies = Cookie.parse(cookieStr || '') || {}
		const cookieCity = cookies['city']
		const cookieCityX = cookies['cityX']
		const cookieCityY = cookies['cityY']
		let payload = {
			city: cookieCity,
			x: cookieCityX,
			y: cookieCityY
		}
		if (cookieCity) {
			commit('setPositionInit', payload)
		}
	},
	async formMail({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/`, formData)

		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async attachmentOrder({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/attachment`, formData)

		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async leasingMail({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/leasing`, formData)
		} catch (e) {
			console.log(e)
			commit('setError', e, {root: true})
		}
	},
	async vacancyMail({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/vacancy/mail`, formData)
		} catch (e) {
			console.log(e)
			commit('setError', e, {root: true})
		}
	},
	async formFooter({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/footer`, formData)

		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async callOrder({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/call`, formData)
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	async dealerForm({commit}, formData) {
		try {
			return await this.$axios.$post(`/api/product/dealer`, formData)
		} catch (e) {
			commit('setError', e, {root: true})
		}
	},
	preloaderAction({commit}) {
		commit('preloaderToggle')
	},
	cityCheckForm({commit}) {
		commit('cityCheckDone')
	},
	videoBgReadyAction({commit}) {
		commit('videoBgReadyChange')
	},
	toClosestDealer({commit}, data) {
		commit('setClosestDealer', data)
	},
	closestDealerCookies({commit}, data) {
		commit('setClosestDealerCookies', data)
	},
	changeAllowScrolling({commit}, data) {
		commit('setAllowScrolling', data)
	},
	// dealers
	async fetchAllDealers({commit}) {
		try{
			let data =  await this.$axios.$get('/api/auth/')
			commit('allDealersSet', data, {root:true})
			return data
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async addDealer({commit}, data) {
		try{
			return await this.$axios.$post('/api/auth/', data)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
	async removeDealer({commit}, id) {
		try{
			return await this.$axios.$delete(`/api/auth/dealers/${id}`)
		} catch(e) {
			commit('setError', e, {root:true})
		}
	},
}

export const mutations = {
	attachmentFilterSet(state, payload) {
		state.attachmentFilterCurrent = payload
	},
	allDealersSet(state, payload){
		state.allDealers = payload
	},
	allVehiclesWithCharsSet(state, payload) {
		state.allVehiclesWithChars = payload
	},
	videoBgReadyChange(state) {
		state.videoBgReady = true
	},
	lastProductListMainSetter(state, payload) {
		state.lastProductListMain = payload
	},
	preloaderToggle(state, payload) {
		state.preloader = false
	},
	modelPageTranslateChange(state, payload) {
		state.modelPageTranslate = payload
	},
	setCookie(state, payload) {
		if (payload.city) {
			Cookies.remove('city')
			Cookies.remove('cityX')
			Cookies.remove('cityY')
			Cookies.set('city', payload.city, {expires: 60 * 60 * 24 * 7})
			Cookies.set('cityX', payload.x, {expires: 60 * 60 * 24 * 7})
			Cookies.set('cityY', payload.y, {expires: 60 * 60 * 24 * 7})
		}
	},
	setPosition(state, payload) {
		state.geoPosition.x = payload.x
		state.geoPosition.y = payload.y
		state.geoPosition.city = payload.city
		state.cookieCity = payload.city
	},
	setPositionInit(state, payload) {
		state.geoPosition.x = payload.x
		state.geoPosition.y = payload.y
		state.geoPosition.city = payload.city
		state.cookieCity = payload.city
	},
	setPositionCity(state, payload) {
		state.geoPosition.city = payload
		state.cookieCity = payload
		if (payload) {
			Cookies.remove('city')
			Cookies.set('city', payload, {expires: 60 * 60 * 24 * 7})
		}
	},
	setCookieCity(state, payload) {
		state.cookieCity = payload
	},
	setAllowScrolling(state, payload) {
		state.allowScrolling = payload
	},
	cityCheckDone(state) {
		state.cityCheckDone = true
	},
	setError(state, error) {
		state.error = error
	},
	clearError(state) {
		state.error = null
	},
	setClosestDealer(state, payload) {
		state.closestDealer = payload
		state.setClosestDealerDone = true
	}
}

export const getters = {
	allowScrolling: state => state.allowScrolling,
	error: state => state.error,
	geoPosition: state => state.geoPosition,
	cookieCity: state => state.cookieCity,
	cityCheckDone: state => state.cityCheckDone,
	closestDealer: state => state.closestDealer,
	modelPageTranslate: state => state.modelPageTranslate,
	preloader: state => state.preloader,
	setPositionDone: state => state.setPositionDone,
	setClosestDealerDone: state => state.setClosestDealerDone,
	lastProductListMain: state => state.lastProductListMain,
	allVehiclesWithChars: state => state.allVehiclesWithChars,
	videoBgReady: state => state.videoBgReady,
	allDealers: state => state.allDealers,
}
