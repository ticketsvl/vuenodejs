export const companyScroll = {
	methods: {
		allowOff() {
			this.$store.dispatch('changeAllowScrolling', false, {root: true})
		},
		allowOn() {
			this.$store.dispatch('changeAllowScrolling', true, {root: true})
		},
		scrollMethod() {
			const route = this.$route
			const router = this.$router

			let thisPage

			if (route.fullPath.indexOf('company') !== -1) thisPage = 'company'
			if (route.fullPath.indexOf('advantages') !== -1 || route.fullPath.indexOf('steps') !== -1 || route.fullPath.indexOf('maintenance') !== -1) thisPage = 'dealer'

			let doCompanyScroll = function (e) {

				// cross-browser wheel delta
				e = window.event || e;
				let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
				let deltaY = e.deltaY

				if (delta === 1){
					if (route.fullPath === '/company/technology'){
						router.push(`/company/about`)
					} else if ( route.fullPath === '/company/dealer') {
						router.push(`/company/technology`)
					} else if ( route.fullPath === '/company/vacancy') {
						router.push(`/company/dealer`)
					}
				} else if (delta === -1) {
					if (route.fullPath === '/company/about') {
						router.push(`/company/technology`)
					} else if (route.fullPath === '/company/technology') {
						router.push(`/company/dealer`)
					} else if (route.fullPath === '/company/dealer') {
						router.push(`/company/vacancy`)
					}
				}
				e.preventDefault();

			};

			let doDealerScroll = function (e) {

				// cross-browser wheel delta
				e = window.event || e;
				let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
				let deltaY = e.deltaY
				if (delta === 1){
					if (route.fullPath === '/dealer/advantages'){
						router.push(`/dealer/steps`)
					} else if ( route.fullPath === '/dealer/maintenance') {
						router.push(`/dealer/advantages`)
					}
				} else if (delta === -1) {
					if (route.fullPath === '/dealer/steps') {
						router.push(`/dealer/advantages`)
					} else if (route.fullPath === '/dealer/advantages') {
						router.push(`/dealer/maintenance`)
					}
				}
				e.preventDefault();

			};


			if (this.$store.getters.allowScrolling) {
				if (thisPage === 'company') doCompanyScroll();
				if (thisPage === 'dealer') doDealerScroll();
				this.allowOff()
				setTimeout(this.allowOn, 300)
			}
		},
	},

	mounted() {
		if (window.addEventListener) {
				window.addEventListener("mousewheel", this.scrollMethod, { passive: false },);
				window.addEventListener("DOMMouseScroll", this.scrollMethod, { passive: false });
		} else {
				window.attachEvent("onmousewheel",this.scrollMethod, { passive: false });
		}

	},
	beforeDestroy() {
		window.removeEventListener("mousewheel", this.scrollMethod, false);
		window.removeEventListener("DOMMouseScroll", this.scrollMethod, false);
	}
}
