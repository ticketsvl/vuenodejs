export const scroll = {
	data() {
		return{
			height: '',
		}
	},
	methods: {
		handleScroll () {
			setTimeout( function () {
				// Your scroll handling here
				let container = document.querySelector('#__layout > div')
				container.style.height = window.innerHeight + 'px';
			}, 100);
			window. scrollTo(0, 0);
		},
		heightDetermine () {
			this.height = window.innerHeight;
		}
	},
	mounted(){
			let container = document.querySelector('#__layout > div')
			container.style.height = window.innerHeight + 'px';
	},
	beforeMount () {
		window.addEventListener("load", function() {
			window. scrollTo(0, 0);
		});
		window.addEventListener("orientationchange", this.handleScroll);
		window.addEventListener("resize", this.handleScroll);
	}

}