export const horizontalScrollWheel = {
	mounted() {
		let scrollContainer = []
		if(this.$refs.ps) {
			if ((this.$refs.ps).$refs.container.classList.contains('ps--active-x')) {
				scrollContainer.push((this.$refs.ps).$refs.container)
			}
		}
		if(this.$refs.ps2) {
			if ((this.$refs.ps2).$refs.container.classList.contains('ps--active-x')) {
				scrollContainer.push((this.$refs.ps2).$refs.container)
			}
		}
		if(this.$refs.ps3) {
			if ((this.$refs.ps3).$refs.container.classList.contains('ps--active-x')) {
				scrollContainer.push((this.$refs.ps3).$refs.container)
			}
		}
		if(this.$refs.ps4) {
			if ((this.$refs.ps4).$refs.container.classList.contains('ps--active-x')) {
				scrollContainer.push((this.$refs.ps4).$refs.container)
			}
		}

		scrollContainer.forEach(item => {
			item.onwheel = e => e.stopPropagation();
			item.addEventListener('wheel', function (event) {
				if (event.deltaMode == event.DOM_DELTA_PIXEL) {
					var modifier = 1;
				} else if (event.deltaMode == event.DOM_DELTA_LINE) {
					var modifier = parseInt(getComputedStyle(this).lineHeight);
				} else if (event.deltaMode == event.DOM_DELTA_PAGE) {
					var modifier = this.clientHeight;
				}
				if (event.deltaY != 0) {
					this.scrollLeft += modifier * event.deltaY;
					event.preventDefault();
				}
			});
		});
	},
}