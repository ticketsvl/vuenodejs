const {Schema, model} = require('mongoose')

const optionSchema = new Schema({
	optionType: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
	},
	images: {
		type: Array,
	},
	forModels:{
		type: Array,
	},
})

module.exports = model('options', optionSchema)