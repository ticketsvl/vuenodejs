const {Schema, model} = require('mongoose')

const dataProductSchema = new Schema({
	model: {
		type: String,
		unique: true,
		required: true
	},
	withVideo: {
		type: Boolean,
	},
	withCatalog: {
		type: Boolean,
	},
	with360inside: {
		type: Boolean,
	},
	with360outside: {
		type: Boolean,
	},
	withGallery: {
		type: Boolean,
	},
	price:{
		type: Number,
	},
	imagesQuantity: {
		type: Number,
	},
	videoLink: {
		type: String,
	},
	seoTitle: {
		type: String,
	},
	seoDescription: {
		type: String,
	}

})

module.exports = model('dataProduct', dataProductSchema)