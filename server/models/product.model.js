const {Schema, model} = require('mongoose')

const productSchema = new Schema({
	model: {
		type: String,
		unique: true,
		required: true
	},
	type: {
		type: String,
		required: true,
	},
	forkType: {
		type: String,
	},
	catalogCardImg: {
		type: String,
		default: 'nophoto.png'
	},
	modelImages: {
		type: Array,
		default: ['nophoto.png'],
	},
	modelVideo:{
		type: String,
	},
	catalogMainSpecifications: {
		type: Object,
		required: true,
	},
	characteristics: Schema.Types.Mixed,
	compatibility:{
		type: Array,
	},
	modelPrice:{
		type: Number,
	},
	modelSet:{
		type: Array,
	}
})

module.exports = model('products', productSchema)