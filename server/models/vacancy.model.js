const {Schema, model} = require('mongoose')

const vacancySchema = new Schema({
	title: {
		type: String,
		required: true
	},
	description: {
		type: String,
	},
	responsibilities: {
		type: String,
	},
	requirements: {
		type: String,
	},
	conditions: {
		type: String,
	},
	salaryFrom: {
		type: Number,
	},
	salaryTo: {
		type: Number,
	}
})

module.exports = model('vacancy', vacancySchema)