const {Schema, model} = require('mongoose')

const newsSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	date: {
		type: String
	},
	block1: {
		type: String,
	},
	seoTitle: {
		type: String
	},
	seoDescription: {
		type: String
	},
})

module.exports = model('questions', newsSchema)