const {Schema, model} = require('mongoose')

const dealerSchema = new Schema({
	x:{
		type: Number,
		required: true
	},
	y:{
		type: Number,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	region: {
		type: String,
		required: true
	},
	city: {
		type: String,
		required: true
	},
	address: {
		type: String,
		required: true,
	},
	imgSrc: {
		type: String,
	},
	mapActive: {
		type: Boolean,
		default: false
	},
	short: {
		type: Boolean,
		default: true
	},
	phone: {
		type: Number,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	mapZoom: {
		type: Number,
		default: 15
	},

})

module.exports = model('dealer', dealerSchema)