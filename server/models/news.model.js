const {Schema, model} = require('mongoose')

const newsSchema = new Schema({
	template: {
		type: String,
		required: true,
	},
	title: {
		type: String,
		required: true
	},
	newsUrl: {
		type: String,
		required: true,
		unique: true,
	},
	date: {
		type: String
	},
	preview: {
		type: String,
	},
	imgList: {
		type: Array,
	},
	block1: {
		type: String,
	},
	block2: {
		type: String,
	},
	block3: {
		type: String,
	},
	block4: {
		type: String,
	},
	seoTitle: {
		type: String
	},
	seoDescription: {
		type: String
	},

})

module.exports = model('news', newsSchema)