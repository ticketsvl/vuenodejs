const {Schema, model} = require('mongoose')

const videoSchema = new Schema({

	videoName: {
		type: String,
		required: true
	},
	pageUrl: {
		type: String,
		required: true,
		unique: true,
	},
	videoUrl: {
		type: String,
		required: true,
		unique: true,
	},
	date: {
		type: String
	},
	seoTitle: {
		type: String
	},
	seoDescription: {
		type: String
	},
})

module.exports = model('video', videoSchema)