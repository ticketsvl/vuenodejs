require('dotenv').config()


const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const logger = require('./logger')
const passport = require('passport')
const passportStrategy = require('./middleware/passport-strategy')
const authRoutes = require('./routes/auth.routes')
const userRoutes = require('./routes/user.routes')
const productRoutes = require('./routes/product.routes')
const mediaRoutes = require('./routes/media.routes')
const app = express()

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true})
	.then( () => {
		console.log('mongo connected')
		logger.log('info', `mongo connected`)
	})
	.catch( (error) => {
		console.error(error)
		logger.log('error', `mongo connecting FAILED, error: ${error}`)
	})

app.use(passport.initialize())
passport.use(passportStrategy)

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use('/api/auth', authRoutes)
app.use('/api/user', userRoutes)
app.use('/api/product', productRoutes)
app.use('/api/media', mediaRoutes)

module.exports = app
