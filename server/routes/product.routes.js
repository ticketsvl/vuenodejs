const passport = require('passport')
const {Router} = require('express')
const ctr = require('../controllers/product.controller')
const router = Router()

router.get(
	'/admin',
	passport.authenticate('jwt', {session: false}),
	ctr.getAll,
)
router.get(
	'/admin/products/:model',
	passport.authenticate('jwt', {session: false}),
	ctr.getByModel
)
router.get(
	'/admin/moreinfo/:model',

	ctr.getMoreInfoByModel
)
router.put(
	'/admin/products/:model',
	passport.authenticate('jwt', {session: false}),
	ctr.updateModel
)
router.post(
	'/admin/moreinfo/',

	ctr.createMoreInfo
)

router.put(
	'/admin/moreinfo/',

	ctr.updateMoreInfo
)
router.post(
	'/admin/option',
	passport.authenticate('jwt', {session: false}),
	ctr.createOption
)
router.delete(
	'/admin/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.remove
)




router.get(
	'/',
	ctr.getAll
)
router.get(
	'/img/:name',
	ctr.getImgNumber
)
router.post(
	'/',
	ctr.formMail
)
router.post(
	'/attachment/',
	ctr.formAttachmentMail
)
router.post(
	'/call',
	ctr.callOrder
)
router.post(
	'/dealer',
	ctr.dealerForm
)
router.post(
	'/footer',
	ctr.footerForm
)
router.post(
	'/leasing',
	ctr.leasingMail
)
router.post(
	'/vacancy/mail',
	ctr.vacancyMail
)
router.get(
	'/options',
	ctr.getAllOptions
)
router.get(
	'/catalog/:type',
	ctr.getByType
)
router.get(
	'/model/:model',
	ctr.getByModel
)

module.exports = router
