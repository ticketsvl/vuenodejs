const passport = require('passport')
const {Router} = require('express')
const ctr = require('../controllers/auth.controller')
const router = Router()


router.post('/admin/login', ctr.login)

router.post(
	'/admin/create',
	passport.authenticate('jwt', {session: false}),
	ctr.createUser
)
router.get(
	'/',
	ctr.getAllDealers
)
router.post(
	'/',
	passport.authenticate('jwt', {session: false}),
	ctr.addDealer
)
router.delete(
	'/dealers/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.removeDealer
)




module.exports = router