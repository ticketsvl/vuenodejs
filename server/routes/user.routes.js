const passport = require('passport')
const {Router} = require('express')
const ctr = require('../controllers/user.controller')
const router = Router()

router.get(
	'/admin',
	passport.authenticate('jwt', {session: false}),
	ctr.getAll
)

module.exports = router