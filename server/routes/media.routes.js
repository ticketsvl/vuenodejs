const passport = require('passport')
const {Router} = require('express')
const ctr = require('../controllers/media.controller')
const router = Router()


router.get(
	'/admin/vacancy',
	passport.authenticate('jwt', {session: false}),
	ctr.getVacancyList
)
router.post(
	'/admin/vacancy',
	passport.authenticate('jwt', {session: false}),
	ctr.createVacancy
)
router.put(
	'/admin/vacancy',
	passport.authenticate('jwt', {session: false}),
	ctr.updateVacancy
)
router.delete(
	'/admin/vacancy/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.removeVacancy
)
router.get(
	'/admin/news',
	passport.authenticate('jwt', {session: false}),
	ctr.getAllNews
)
router.get(
	'/admin/articles',
	passport.authenticate('jwt', {session: false}),
	ctr.getAllArticles
)
router.post(
	'/admin/news',
	passport.authenticate('jwt', {session: false}),
	ctr.createNews
)
router.post(
	'/admin/articles',
	passport.authenticate('jwt', {session: false}),
	ctr.createArticle
)
router.post(
	'/admin/video',
	passport.authenticate('jwt', {session: false}),
	ctr.createVideo
)
router.put(
	'/admin/video',
	passport.authenticate('jwt', {session: false}),
	ctr.updateVideo
)
router.put(
	'/admin/news',
	passport.authenticate('jwt', {session: false}),
	ctr.updateNews
)
router.put(
	'/admin/articles',
	passport.authenticate('jwt', {session: false}),
	ctr.updateArticle
)
router.put(
	'/admin/questions',
	passport.authenticate('jwt', {session: false}),
	ctr.updateQuestion
)
router.delete(
	'/admin/questions/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.removeQuestion
)
router.delete(
	'/admin/news/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.removeNews
)
router.delete(
	'/admin/articles/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.removeArticle
)
router.delete(
	'/admin/video/:id',
	passport.authenticate('jwt', {session: false}),
	ctr.removeVideo
)
router.post(
	'/admin/questions',
	passport.authenticate('jwt', {session: false}),
	ctr.createQuestion
)


router.get(
	'/news',
	ctr.getAllNews
)
router.get(
	'/vacancy',
	ctr.getVacancyList
)
router.get(
	'/vacancy/:id',
	ctr.getVacancyByUrl
)
router.get(
	'/articles',
	ctr.getAllArticles
)
router.get(
	'/video',
	ctr.getAllVideos
)
router.get(
	'/news/:id',
	ctr.getNewsByUrl
)
router.get(
	'/articles/:id',
	ctr.getArticleByUrl
)
router.get(
	'/video/:id',
	ctr.getVideoByUrl
)
router.get(
	'/questions',
	ctr.getAllQuestions
)
router.get(
	'/questions/:id',
	ctr.getQuestionByUrl
)

module.exports = router