const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const keys = require('../keys')
const User = require('../models/user.model')
const Dealer = require('../models/dealer.model')


module.exports.login = async (req, res) => {
	const candidate = await User.findOne({login:req.body.login})

	if (candidate) {
		const isPasswordCorrect = bcrypt.compareSync(req.body.password, candidate.password)

		if (isPasswordCorrect) {
			const token = jwt.sign({
				login: candidate.login,
				userId: candidate._id
			}, keys.JWT, {expiresIn: 60 * 60 * 3})
			res.json({token})
		} else {
			res.status(401).json({message: 'Пароль не верный'})
		}



	} else {
		res.status(404).json({message: 'Пользователь не найден'})
	}
}
module.exports.createUser = async (req, res) => {
	const candidate = await User.findOne({login: req.body.login})

	if (candidate) {
		res.status(409).json({message: 'Логин уже занят'})
	} else {
		const salt = bcrypt.genSaltSync(10)
		const user = new User({
			login: req.body.login,
			password: bcrypt.hashSync(req.body.password, salt)

		})

		await user.save()
		res.status(201).json(user)
	}

}

module.exports.getAllDealers = async (req, res) => {
	try {
		const dealer = await Dealer.find()
		res.json(dealer)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.addDealer = async (req, res) => {
		const dealer = new Dealer({
			title: req.body.title,
			x: req.body.x,
			y: req.body.y,
			name: req.body.name,
			city: req.body.city,
			address: req.body.address,
			phone: req.body.phone,
			email: req.body.email,
			region: req.body.region,
			mapActive: req.body.mapActive,
			mapZoom: req.body.mapZoom,
			short: req.body.short,
			imgSrc: req.body.imgSrc
		})
		try {
			await dealer.save()
			res.status(201).json(dealer)
		} catch (e) {
			console.log(e)
			res.status(500).json(e)
		}
	}
module.exports.removeDealer = async (req, res) => {
	try {
		await Dealer.deleteOne({_id: req.params.id})
		res.json({message: 'удален'})
	} catch (e) {
		res.status(500).json(e)
	}
}