const User = require('../models/user.model')

module.exports.getAll = async(req, res) => {
	try {
		const user = await User.find()
		res.json(user)
	} catch (e) {
		res.status(500).json(e)
	}
}
