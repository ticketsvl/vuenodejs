const mongoose = require('mongoose')
const mailer = require('../nodemailer')
const logger = require('../logger')
const News = require('../models/news.model.js')
const Vacancy = require('../models/vacancy.model.js')
const Article = require('../models/article.model.js')
const Video = require('../models/video.model.js')
const Questions = require('../models/questions.model.js')

module.exports.getVacancyList = async (req, res) => {
	try {
		const vacancy = await Vacancy.find()
		res.json(vacancy)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getVacancyByUrl = async (req, res) => {
	try {
		const vacancy = await Vacancy.find(
			{"_id": req.params.id}
		)
		res.json(vacancy)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.createVacancy = async (req, res) => {
	const vacancy = new Vacancy({
		title: req.body.title,
		description: req.body.description,
		salaryFrom: req.body.salaryFrom,
		salaryTo: req.body.salaryTo,
		responsibilities: req.body.responsibilities,
		requirements: req.body.requirements,
		conditions: req.body.conditions
	})
	try {
		await vacancy.save()
		res.status(201).json(vacancy)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.updateVacancy = async(req, res) => {
	const $set = {
		title: req.body.title,
		description: req.body.description,
		salaryFrom: req.body.salaryFrom,
		salaryTo: req.body.salaryTo,
		responsibilities: req.body.responsibilities,
		requirements: req.body.requirements,
		conditions: req.body.conditions
	}
	try {
		const vacancy = await Vacancy.findOneAndUpdate({
			_id: req.body.id
		}, {$set}, {new: true})
		res.json(vacancy)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.removeVacancy = async (req, res) => {
	try{
		await Vacancy.deleteOne({_id: req.params.id})
		res.json({message: 'удален'})
	} catch (e) {
		res.status(500).json(e)
	}
}

module.exports.getAllNews = async (req, res) => {
	try {
		const news = await News.find()
		res.json(news)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getNewsByUrl = async (req, res) => {
	try {
		const newsById = await News.find(
			{"newsUrl": req.params.id}
		)
		res.json(newsById)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.createNews = async (req, res) => {
	const news = new News({
		title: req.body.title,
		newsUrl: req.body.newsUrl,
		date: req.body.date,
		template: req.body.template,
		preview: req.body.preview,
		imgList: req.body.imgList,
		block1: req.body.block1,
		block2: req.body.block2,
		block3: req.body.block3,
		block4: req.body.block4,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription
	})
	try {
		await news.save()
		res.status(201).json(news)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.removeNews = async (req, res) => {
	try{
		await News.deleteOne({_id: req.params.id})
		res.json({message: 'удален'})
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.removeQuestion = async (req, res) => {
	try{
		await Questions.deleteOne({_id: req.params.id})
		res.json({message: 'удален'})
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.updateNews = async(req, res) => {
	const $set = {
		newsUrl: req.body.newsUrl,
		template: req.body.template,
		title: req.body.title,
		date: req.body.date,
		block1: req.body.block1,
		block2: req.body.block2,
		block3: req.body.block3,
		block4: req.body.block4,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
	}
	try {
		const news = await News.findOneAndUpdate({
			newsUrl: req.body.newsUrl
		}, {$set}, {new: true})
		res.json(news)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}

module.exports.getAllArticles = async (req, res) => {
	try {
		const articles = await Article.find()
		res.json(articles)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getArticleByUrl = async (req, res) => {
	try {
		const articlesById = await Article.find(
			{"newsUrl": req.params.id}
		)

		res.json(articlesById)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.createArticle = async (req, res) => {
	const article = new Article({
		title: req.body.title,
		newsUrl: req.body.newsUrl,
		date: req.body.date,
		template: req.body.template,
		preview: req.body.preview,
		imgList: req.body.imgList,
		block1: req.body.block1,
		block2: req.body.block2,
		block3: req.body.block3,
		block4: req.body.block4,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription
	})
	try {
		await article.save()
		res.status(201).json(article)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.updateArticle = async(req, res) => {
	const $set = {
		newsUrl: req.body.newsUrl,
		template: req.body.template,
		title: req.body.title,
		date: req.body.date,
		block1: req.body.block1,
		block2: req.body.block2,
		block3: req.body.block3,
		block4: req.body.block4,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
	}
	try {
		const article = await Article.findOneAndUpdate({
			newsUrl: req.body.newsUrl
		}, {$set}, {new: true})
		res.json(article)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.removeArticle = async (req, res) => {
		try {
			await Article.deleteOne({_id: req.params.id})
			res.json({message: 'удален'})
		} catch (e) {
			res.status(500).json(e)
		}
	}

module.exports.getAllVideos = async (req, res) => {
	try {
		const video = await Video.find()
		res.json(video)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getVideoByUrl = async (req, res) => {
	try {
		const videoById = await Video.find(
			{"pageUrl": req.params.id}
		)

		res.json(videoById)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.createVideo = async (req, res) => {
	const video = new Video({
		videoName: req.body.videoName,
		pageUrl: req.body.pageUrl,
		videoUrl: req.body.videoUrl,
		date: req.body.date,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
	})
	try {
		await video.save()
		res.status(201).json(video)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.removeVideo = async (req, res) => {
	try {
		await Video.deleteOne({_id: req.params.id})
		res.json({message: 'удален'})
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.updateVideo = async(req, res) => {
	const $set = {
		pageUrl: req.body.pageUrl,
		videoName: req.body.videoName,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
		videoUrl: req.body.videoUrl,
		date: req.body.date,
	}
	try {
		const video = await Video.findOneAndUpdate({
			pageUrl: req.body.pageUrl
		}, {$set}, {new: true})

		res.json(video)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}

module.exports.getAllQuestions = async (req, res) => {
	try {
		const questions = await Questions.find()
		res.json(questions)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getQuestionByUrl = async (req, res) => {
	try {
		const questionById = await Questions.find(
			{"url": req.params.id}
		)

		res.json(questionById)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.createQuestion = async (req, res) => {
	const question = new Questions({
		title: req.body.title,
		date: req.body.date,
		template: req.body.template,
		preview: req.body.preview,
		imgList: req.body.imgList,
		block1: req.body.block1,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription
	})
	try {
		await question.save()
		res.status(201).json(question)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.updateQuestion = async(req, res) => {
	const $set = {
		url: req.body.url,
		title: req.body.title,
		date: req.body.date,
		block1: req.body.block1,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
	}
	try {
		const question = await Questions.findOneAndUpdate({
			url: req.body.url
		}, {$set}, {new: true})
		res.json(question)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}