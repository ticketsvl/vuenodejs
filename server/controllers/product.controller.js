const mailer = require('../nodemailer')
const Product = require('../models/product.model')
const DataProduct = require('../models/dataproduct.model')
const Option = require('../models/option.model')
const fs = require('fs');

module.exports.getImgNumber = async (req, res) => {
	const dir = `./static/img/models/${req.params.name}/player/gallery`;
	try {
		let number1
		fs.readdir(dir, (err, files) => {
			if(files){
				number1 = files.length
			} else {
				number1 = 0
			}

			res.json(number1)
		});

	} catch (e) {
		res.status(500).json(e)
	}
}

module.exports.getAll = async (req, res) => {
	try {
		const product = await Product.find()
		res.json(product)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getAllOptions = async (req, res) => {
	try {
		const options = await Option.find()
		res.json(options)
	} catch (e) {
		res.status(500).json(e)
	}
}

module.exports.createProduct = async (req, res) => {
	const product = new Product({
		model: req.body.modelName,
		type: req.body.type,
		forkType: req.body.forkType,
		catalogCardImg: req.body.catalogCardImg,
		modelImages: req.body.modelImages,
		modelVideo: req.body.modelVideo,
		catalogMainSpecifications: {
			bucketCapacity: req.body.bucketCapacity,
			carryingCapacity: req.body.carryingCapacity,
			weight: req.body.weight,
			dischargeHeight: req.body.dischargeHeight,
			liftingHeight: req.body.liftingHeight,
			diggingDepth: req.body.diggingDepth,
		},
		characteristics: req.body.characteristics,
		modelPrice: req.body.modelPrice,
	})
	try {
		await product.save()
		res.status(201).json(product)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.createMoreInfo = async (req, res) => {
	const dataProduct = new DataProduct({
		model: req.body.model,
		price: req.body.price,
		withVideo: req.body.withVideo,
		videoLink:req.body.videoLink,
		with360inside: req.body.with360inside,
		with360outside: req.body.with360outside,
		imagesQuantity: req.body.imagesQuantity,
		withGallery: req.body.withGallery,
		withCatalog: req.body.withCatalog,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
	})
	try {
		await dataProduct.save()
		res.status(201).json(dataProduct)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.createOption = async (req, res) => {
	const option = new Option({
		optionType: req.body.optionType,
		name: req.body.name,
		description: req.body.description,
		images: req.body.images,
		forModels: req.body.models,
	})
	try {
		await option.save()
		res.status(201).json(option)
	} catch (e) {
		res.status(500).json(e)
	}
}

module.exports.getByType = async (req, res) => {
	try {
		const product = await Product.find(
			{"type": req.params.type}
		)
		res.json(product)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getByModel = async (req, res) => {
	try {
		const product = await Product.find(
			{"model": req.params.model.toUpperCase()}
		)
		res.json(product)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.getMoreInfoByModel = async (req, res) => {
	try {
		const moreInfo = await DataProduct.find(
			{"model": req.params.model.toUpperCase()}
		)
		res.json(moreInfo)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}

module.exports.updateModel = async(req, res) => {
	const $set = {
		model: req.body.model,
		type: req.body.type,
		catalogMainSpecifications:{
			bucketCapacity: req.body.bucketCapacity,
			carryingCapacity: req.body.carryingCapacity,
			weight: req.body.weight,
			dischargeHeight: req.body.dischargeHeight,
			liftingHeight: req.body.liftingHeight,
			diggingDepth: req.body.diggingDepth,
		},
		catalogCardImg: req.body.catalogCardImg,
		characteristics: req.body.characteristics,
		modelImages: req.body.modelImages,
		modelPrice: req.body.modelPrice,
		modelVideo: req.body.modelVideo
	}
	try {
		const product = await Product.findOneAndUpdate({
			model: req.params.model.toUpperCase()
		}, {$set}, {new: true})

		res.json(product)
	} catch (e) {
		res.status(500).json(e)
	}
}
module.exports.updateMoreInfo = async(req, res) => {
	const $set = {
		model: req.body.model,
		price: req.body.price,
		withVideo: req.body.withVideo,
		videoLink:req.body.videoLink,
		with360inside: req.body.with360inside,
		with360outside: req.body.with360outside,
		imagesQuantity: req.body.imagesQuantity,
		withGallery: req.body.withGallery,
		withCatalog: req.body.withCatalog,
		seoTitle: req.body.seoTitle,
		seoDescription: req.body.seoDescription,
	}
	try {
		const moreInfo = await DataProduct.findOneAndUpdate({
			model: req.body.model.toUpperCase()
		}, {$set}, {new: true})

		res.json(moreInfo)
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.remove = async (req, res) => {
	try {
		await Product.deleteOne({_id: req.params.id})
		res.json({message: 'удален'})
	} catch (e) {
		res.status(500).json(e)
	}
}

module.exports.formMail = async (req, res) => {
	try {
		let optList = []
		let attList = []
		if (req.body.formData.options.length > 0){
			req.body.formData.options.forEach((item)=> optList.push(item.name))
		}
		if (req.body.formData.attachments.length > 0) {
			req.body.formData.attachments.forEach((item) => attList.push(item.name))
		}

		let name = req.body.name
		let phone = req.body.phone
		if (req.body.mail) {
			let mail = req.body.mail
		}
		let data = req.body.formData
		let comment = 'Пользователь не оставил комментарий'
		if (req.body.text) {
			comment = req.body.text
		}

		const message = {
			to: 'info@boulder.com.ru, com.eurasia@gmail.com',
      subject: 'Расчет цены',
			text:`
Имя: ${req.body.name},
Телефон: ${req.body.phone},
E-mail: ${req.body.mail},
Комментарий клиента: ${comment}

============

Интересующая модель: ${req.body.formData.item.Name} / ${req.body.formData.item.VehicleTypeRus}

Опций добавлено клиентом - ${optList.length}:
${optList},

============

Навесное оборудование добавленое клиентом - ${attList.length}:
${attList},`
}

		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.formAttachmentMail = async (req, res) => {
	try {

		const message = {
			to: 'info@boulder.com.ru, com.eurasia@gmail.com',
			subject: 'Расчет цены и сроков доставки Навесного оборудования',
			text:`
Имя: ${req.body.name},
Телефон: ${req.body.phone},
Навесное: ${req.body.attachment},
Модель: ${req.body.vehicle}`
}

		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.callOrder = async (req, res) => {
	try {
		const message = {
			to: 'info@boulder.com.ru, com.eurasia@gmail.com',
      subject: 'Запрос звонка',
			text:`
Имя: ${req.body.name},
телефон: ${req.body.phone},`
}
		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.dealerForm = async (req, res) => {
	try {
		const message = {
			to: 'komdir@buyloader.ru, com.eurasia@gmail.com',
			subject: 'Стать дилером',
			text:`
Имя: ${req.body.name},
телефон: ${req.body.phone},
емэйл: ${req.body.mail},
ИНН: ${req.body.inn},`
		}
		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.footerForm = async (req, res) => {
	try {
		const message = {
			to: 'info@boulder.com.ru, com.eurasia@gmail.com',
			subject: 'Вопрос с сайта Boulder',
			text:`
Имя: ${req.body.name},
Email: ${req.body.mail},
Телефон: ${req.body.phone},
Текст: ${req.body.text},`
		}
		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
module.exports.vacancyMail = async (req, res) => {
	try {

		const message = {
			to: 'job@boulder.com.ru',
			subject: `Отклик на вакансию Boulder - ${req.body.vacancy}`,
			text:`
Вакансия: ${req.body.vacancy},
Имя: ${req.body.name},
Телефон: ${req.body.phone},
Email: ${req.body.mail},
Резюме: ${req.body.resumeLink}`
		}

		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
},
module.exports.leasingMail = async (req, res) => {
	try {
		let type = req.body.type.name
		let model = req.body.model
		let company = req.body.leasingCompany
		let name = req.body.user.name
		let city = req.body.user.city
		let phone = req.body.user.phone

		let comment = 'Пользователь не оставил комментарий'

		if (req.body.user.comment) {
			comment = req.body.user.comment
		}

		const message = {
			to: 'info@boulder.com.ru, com.eurasia@gmail.com',
			//to: 'blackbim123@gmail.com',
			subject: 'Заявка на лизинг',
			text:`
Быстрая заявка на лизинг:

${type}: ${model}
Интересующая лизинговая компания: ${company}

Информация о пользователе:
Имя: ${name}
Город: ${city}
Телефон: ${phone}
Комментарий: ${comment}`}
		mailer(message)
		res.json({message: 'Отправлен'})
	} catch (e) {
		console.log(e)
		res.status(500).json(e)
	}
}
