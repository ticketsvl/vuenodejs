import Vue from 'vue'
import YmapPlugin from 'vue-yandex-maps'

const settings = {
	apiKey: '3803b216-9a26-4d92-b91c-71dafa639e6b',
	lang: 'ru_RU',
}

Vue.use(YmapPlugin, settings);