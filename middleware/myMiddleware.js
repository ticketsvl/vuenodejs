const redirects = require('../301.json')

export default function(ctx) {

	const redirect = redirects.find(r => r.from === ctx.route.fullPath)
	if (redirect) {
    ctx.redirect(301, redirect.to)
  }
}
